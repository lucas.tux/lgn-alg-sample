# Command line instructions
You can also upload existing files from your computer using the instructions below.

# Git global setup

```bash
git config --global user.name "Lucas Gonçalves"
git config --global user.email "lucas.tux@gmail.com"
```

# Create a new repository

```bash
git clone https://gitlab.com/lucas.tux/lgn-alg-sample.git
cd lgn-alg-sample
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

# Push an existing folder

```bash
cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/lucas.tux/lgn-alg-sample.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

# Push an existing Git repository

```bash
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/lucas.tux/lgn-alg-sample.git
git push -u origin --all
git push -u origin --tags
```
